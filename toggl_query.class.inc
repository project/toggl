<?php

/**
 * Builds and executes queries for the Redmine REST API.
 *
 * @todo Postponed: Support authentication by username & password too.
 *
 * @see http://www.toggl.com/public/api
 * @see toggl_api_query()
 */
class toggl_query extends rest_api_query {
  protected $password = 'api_token';
  protected $scheme = 'https';
  protected $host = 'www.toggl.com';
  protected $path = 'api/v3';

  /**
   * Constructor requires a valid API key and access point URL to initialize.
   *
   * These can be set with Drupal variables instead.  E.g. In settings.php:
   * @code
   * $conf['toggl_api_key'] = 'VALUE';
   * @code
   *
   * @param $api_key String
   *    The API key to authenticate with Redmine via the API.  Defaults to 
   *    variable_get('toggl_api_key')
   */
  function __construct($api_key = NULL) {
    // Was the API key passed in?
    if (isset($api_key)) {
      // Yes;  Set the API key.
      $this->api_key = $api_key;
    }
    else {
      // No;  Is it set in Drupal's configuration?
      if ($variable = variable_get('toggl_api_token', FALSE)) {
        // Yes;  Set the property.
        $this->api_key = $variable;
      }
      else {
        // No;  Fail.
        $args = array('!conf' => conf_path());
        $message = "Toggl.com API needs the <code>toggl_api_token</code> to use the REST API.  Set <code>\$conf['toggl_api_token'] = 'VALUE';</code> in <code>!conf/settings.php</code>.";

        // Set the message on this object, as a drupal message and with watchdog.
        $this->error = t($message, $args);
        drupal_set_message($this->error, 'error');
        watchdog('redmine', $message, $args, WATCHDOG_ERROR);

        // Initialization failed.  Stop trying.
        return;
      }
    }

    // Initialization succeeded.  Permit this object to be executed.
    $this->initialized = TRUE;
  }
}
